local types = require("./tm_modules/types")
local protocol = require("./tm_modules/protocol")

function tohex(input)
  result = string.format('%X', packet_id);
  if(#result == 1) then
    return "0" .. result
  else
    return result
  end
end

-- Main

function dissect_packet(buffer, pinfo, tree, offset)
  -- Packet length, VarInt
  packet_length = types.varint(buffer, offset + 0, subtree);
  var_length = types.varint_length(buffer, offset + 0);
  pointer = 0;
  tree:add(buffer(pointer, var_length),"Packet Length: " .. packet_length);
  pointer = var_length;
  -- Packet ID, VarInt
  packet_id = types.varint(buffer, offset + pointer, subtree);
  var_length = types.varint_length(buffer, offset + pointer);
  tree:add(buffer(pointer, var_length),"Packet ID: 0x" .. tohex(packet_id));
  pointer = pointer + var_length;
  -- Data, ByteArray
  tree:add(buffer(pointer, packet_length - var_length - pointer),"Data: " .. buffer(pointer, packet_length - var_length - pointer));
 
end


-- minecraft protocol
-- declare our protocol
minecraft_proto = Proto("minecraft","Minecraft Protocol");

-- create a function to dissect it
function minecraft_proto.dissector(buffer,pinfo,tree)
  pinfo.cols.protocol = "MINECRAFT"
  local subtree = tree:add(minecraft_proto,buffer(),"Minecraft Protocol");
  dissect_packet(buffer, pinfo, subtree, 0);
end


function minecraft_proto.init()
  protocol.parse_protocol(340);

  -- load the tcp.port table
  tcp_port_table = DissectorTable.get("tcp.port");
  -- register our protocol to handle tcp port 25565
  tcp_port_table:add(25565,minecraft_proto);
end
