local protocol = {};
local json = require("json");

local function read_file(path)
  local file = io.open(path, "rb") -- r read mode and b binary mode
  if not file then return nil end
  local content = file:read "*a" -- *a or *all reads the whole file
  file:close()
  return content
end

function protocol.parse_protocol(version)
  versions = json.decode(read_file("./minecraft-data/data/pc/common/protocolVersions.json"));
  for k, v in ipairs(versions) do
    if(v["version"] == version) then
      print(v["minecraftVersion"]);
    end
  end
end

function protocol.parse_packet(version, id, state, bound_to)

end

return protocol
