local types = {};

local bit32 = require("bit32");

-------------------
-- Bit functions --
-------------------

function bit(p)
  return 2 ^ (p - 1)  -- 1-based indexing
end

-- Typical call:  if hasbit(x, bit(3)) then ...
function hasbit(x, p)
  return x % (p + p) >= p;
end

------------
-- VarInt --
------------

function types.varint_length(buffer, offset)
  count = 0;
  read = 0;
  while ( count == 0 or hasbit(read, bit(8)) )
  do
    read = buffer(offset + count, 1):uint();
    count = count + 1;
  end
  return count;
end

function types.varint(buffer, offset, subtree)
  value = 0;
  count = 0;
  read = 0;
  while ( count == 0 or hasbit(read, bit(8)) )
  do
    read = buffer(offset + count, 1):uint();
    temp = bit32.band(read, 127); -- 127 = 0b01111111 = 0x7F
    temp = bit32.lshift(temp, 7 * count);
    value = bit32.bor(value, temp);
    count = count + 1;
  end
  return value;
end

return types
